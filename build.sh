#!/bin/bash -xe

cd cx_Oracle-packager
docker build -t lambda-cxoracle-deploy .
cd -

mkdir -p dist
rm -rf dist/*

cp -r lambda/* dist/

cd dist
docker run --name lambda-cxoracle-deploy lambda-cxoracle-deploy
docker cp lambda-cxoracle-deploy:/tmp/lib.tar.gz lib.tar.gz
docker rm lambda-cxoracle-deploy

tar xvzf lib.tar.gz
rm lib.tar.gz
zip -9 -r lambda.zip ./*
cd ..
