#!/bin/bash -xe

cd /tmp

rm -rf /tmp/lib
rm -rf /tmp/venv

virtualenv --python=python2.7 venv
source venv/bin/activate

export LD_LIBRARY_PATH=/usr/lib/oracle/12.1/client64/lib/:${LD_LIBRARY_PATH}
pip install cx_Oracle -t lib
ldd lib/cx_Oracle.so| grep "=> /" | awk '{print $3}' | xargs -I '{}' cp -v '{}' lib/

cp lib/libclntsh.so.12.1 lib/libclntsh.so
cp /usr/lib/oracle/12.1/client64/lib/libociei.so lib/
cp /usr/lib/oracle/12.1/client64/lib/libocci.so.12.1 lib/
cp /usr/lib/oracle/12.1/client64/lib/libocci.so.12.1 lib/libocci.so

tar cvzf lib.tar.gz lib
