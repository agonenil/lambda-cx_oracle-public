from __future__ import print_function

import logging
import os

import sys

sys.path.append('lib')
import cx_Oracle as dbapi

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# This is required for Oracle to generate an OID
# The lambda also requires an environment variable of
# HOSTALIASES=/tmp/HOSTALIASES
with open('/tmp/HOSTALIASES', 'w') as hosts_file:
    hosts_file.write('{} localhost\n'.format(os.uname()[1]))


def handler(event, context):

    logger.info('LD_LIBRARY_PATH: {}'.format(os.environ['LD_LIBRARY_PATH']))
    logger.info('ORACLE_HOME: {}'.format(os.environ['ORACLE_HOME']))

    # Connect to the database
    logger.info('Connecting to the database')
    db_connection = dbapi.connect('{user}/{password}@{host}:{port}/{database}'
                                  .format(user=os.environ['DB_USER'],
                                          password=os.environ['DB_PASSWORD'],
                                          host=os.environ['DB_HOSTNAME'],
                                          port=os.environ.get('DB_PORT', '1521'),
                                          database=os.environ['DB_DATABASE']))

    logger.info('Connecting to the database - success')
    cursor = db_connection.cursor()
    try:
        cursor.execute('SELECT \'hello, world\' FROM DUAL')
    except dbapi.DatabaseError, e:
        logging.error('Database error: {}'.format(str(e)))
        raise e
    finally:
        db_connection.close()

    return {
        'result': 'success',
        'cx_Oracle.version': dbapi.version
    }


if __name__ == "__main__":
    print(handler({}, {}))
